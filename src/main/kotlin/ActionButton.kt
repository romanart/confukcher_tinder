import kotlinx.html.TagConsumer
import kotlinx.html.div
import kotlinx.html.img
import kotlinx.html.js.onClickFunction
import org.w3c.dom.HTMLElement
import org.w3c.dom.events.Event

class ActionButton(private val kind: String, private val message: String, private val callback: (String) -> Unit) {

    private val onClickCallback: (Event) -> Unit = {
        callback(message)
    }

    fun TagConsumer<HTMLElement>.render() {
        div(classes = "profile_action_button_box profile_action_button_box_$kind") {
            div(classes = "profile_action_button profile_action_button_$kind") {
                img(src = "svg/${kind}.svg") {
                    onClickFunction = onClickCallback
                }
            }
        }
    }
}